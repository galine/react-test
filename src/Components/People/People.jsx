import React from "react";
import withPeople from "../hoc/WithPeople";
import Person from "./Person/Person";

const People = ({people}) => {
  return (
    <ul className="list">
      {people.map((person, index) => {
        return (
          <Person key={index} index={index} value={person}/>
        )
      })}
    </ul>
  )
}

export default withPeople(People);
