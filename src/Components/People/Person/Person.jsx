import React from "react";
import { Link } from 'react-router-dom';
import withPeople from "../../hoc/WithPeople";
import Button from '@material-ui/core/Button';

const Person = ({value, index, removePerson}) => {

  return (
    <li className="people-list">
        <div>First name: {value.firstname}</div>
        <div>Last name: {value.lastname}</div>
        <div>Email: {value.email}</div>
        <div>
          <Link to={`/people/edit/${index}`} index={index} className="btn" >Edit</Link>
          <Button variant="contained" color="secondary" onClick={() => removePerson(index)}>Remove</Button>
        </div>
    </li>
  )
}

export default withPeople(Person);
