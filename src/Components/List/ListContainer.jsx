import React from "react";
import List from "./List";
import withFields from "../hoc/WithField";

function ListContainer({fields}) {
  return (
    <ul className="list">
      {fields.map((field, index) => {
        return <List key={index} index={index} value={field}/>
      })}
    </ul>
  )
}

export default withFields(ListContainer);