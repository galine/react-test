import React from "react";

function List({value}) {
  return (
    <li>
      <div>firstname: {value.firstname}</div>
      <div>lastname: {value.lastname}</div>
      <div>email: {value.email}</div>
    </li>
  )
}

export default List;