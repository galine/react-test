import React, {Fragment} from "react";
import withPeople from "../hoc/WithPeople";
import EditPerson from "./EditPerson/EditPerson.jsx";
import {useParams} from "react-router-dom";

const Edit = ({people}) => {
  const { index } = useParams();
  const person = people[index];
  return (
    <div>
      <EditPerson label="First name:" id="firstname" index={index} value={person.firstname} name="firstname"/>
      <EditPerson label="Last name:" id="lastname" index={index} value={person.lastname} name="lastname" />
      <EditPerson label="Email:" id="email" index={index} value={person.email} name="email" />
    </div>
  )
}

export default withPeople(Edit);
