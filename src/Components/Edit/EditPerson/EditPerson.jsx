import React from "react";
import withPeople from "../../hoc/WithPeople";

const EditPerson = ({value, index, editField, setEditField, name, type = 'text', id, label}) => {
  return (
    <div>
      <label htmlFor={id}>{label}</label> <br />
      <input id={id} type={type} value={value} index={index} name={name} onChange={(e) => setEditField(index, name, e.target.value)}/>
    </div>
  )
}

export default withPeople(EditPerson);