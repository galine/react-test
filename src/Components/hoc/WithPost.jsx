import React from "react";
import {connect} from "react-redux";
//import {} from "../../Store/action";

let mapStateToProps = (state) => ({
  post: state.post.post,
  isLoading: state.post.isLoading,
});

export default Component => connect(mapStateToProps)(Component)