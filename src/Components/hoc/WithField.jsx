import React from "react";
import {connect} from "react-redux";
import {addNewField, removeField, setField, clearInput, saveField} from "../../Store/action";

let mapStateToProps = (state) => ({
  fields: state.people.fields,
});

export default Component => connect(mapStateToProps, {addNewField, removeField, setField, clearInput, saveField})(Component)
