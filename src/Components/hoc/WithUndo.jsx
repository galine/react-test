import React from "react";
import {connect} from "react-redux";
import {undo} from "../../Store/action";

let mapStateToProps = (state) => ({
  undoData: state.people.undo,
});

export default Component => connect(mapStateToProps, {undo})(Component)
