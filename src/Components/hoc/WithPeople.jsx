import React from "react";
import {connect} from "react-redux";
import {fillField, removeAll, editField, setEditField, removePerson} from "../../Store/action";

let mapStateToProps = (state) => ({
  people: state.people.people,
});

export default Component => connect(mapStateToProps, {fillField, removeAll, editField, setEditField, removePerson})(Component)
