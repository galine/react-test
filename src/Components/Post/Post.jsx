import React, {Fragment} from 'react'
import { useDispatch } from 'react-redux'
import Button from '@material-ui/core/Button';
import {FETCH_POSTS} from "../../Store/action-types";
import withPost from '../../Components/hoc/WithPost'

export const Post = ({post, isLoading}) => {
  const dispatch = useDispatch()


  return (
    <div>
      <Button variant="contained" color="primary"  onClick={() => dispatch({type: FETCH_POSTS})}>
        GET
      </Button>
      {console.log(post)}
      {console.log(isLoading)}
      {isLoading
        ?
          <div>Loading</div>
        : null

      }
      {post.map(({title}, index) => {
        return (
          <ul key={index}>
            <li>{title}</li>
          </ul>
        )
      })}
    </div>
  )
}

export default withPost(Post);