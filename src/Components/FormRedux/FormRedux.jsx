import React, {Fragment, useState} from "react";
import Input from "./Input/Input";
import withFields from "../hoc/WithField";
import withPeople from "../hoc/WithPeople";
import withUndo from "../hoc/WithUndo";
import Button from '@material-ui/core/Button';
import Select from '@material-ui/core/Select';
import MenuItem from '@material-ui/core/MenuItem';
import {makeStyles} from "@material-ui/core";

const useStyles = makeStyles({
  selectStyle: {
    width: 200
  },
  button: {
    margin: 20
  }
})

const FormRedux = ({fields, addNewField, removeField, saveField, people, fillField, removeAll, undo, undoData}) => {
const style = useStyles();

  const [isPasswordVisible, setPasswordVisible] = useState(false);

  const isUndoData = undo.length > 0;

  const lastFields = fields[fields.length - 1];

  let isLastFieldsFilled = true;
  for (let key in lastFields) {
    if(!lastFields[key]){
      isLastFieldsFilled = false;
    }
  }

  return (
    <div>
      <label htmlFor="select">Select name:</label> <br />
      <Select className={style.selectStyle} labelId="demo-simple-select-label" name="select" onChange={(e) => fillField(e.target.value)}>
        {people.map((person, index) => {
          return (
              <MenuItem key={index} width={300} value={index}> {person.firstname} {person.lastname}</MenuItem>
          )
        })}
      </Select>
      <div>
        {fields.map((field, index) => {
          return (
            <Fragment key={index}>{console.log(fields)}
              <Input label="First name:" id="firstname" index={index} value={field.firstname} name="firstname"/>

              <Input label="Last name:" id="lastname" index={index} value={field.lastname} name="lastname" />

              <Input label="Email:" id="email" index={index} value={field.email} name="email" />

              <Input label="Password:" id="password" type={isPasswordVisible ? null : "password"} index={index} value={field.password} name="password" />

              <Button className={style.button} variant="contained" onClick={() => setPasswordVisible(!isPasswordVisible)}>Toggle mask</Button>

              <Button className={style.button} variant="contained" onClick={() => removeField(index)}>Remove</Button>
            </Fragment>
          )
        })}
      </div>

      {isLastFieldsFilled
        ?
          <div>
            <Button className={style.button} variant="contained" onClick={() => addNewField()}>Add field(Redux)</Button>
            {lastFields
              ?
              <Fragment>
                <Button className={style.button} variant="contained" color="primary" onClick={() => saveField()}>Save</Button>
                <Button className={style.button} variant="contained" color="secondary" onClick={() => removeAll()}>Remove All</Button>
              </Fragment>
              : null}
          </div>
        : null}

      {isUndoData
        ?
          <Button className={style.button} variant="contained" onClick={() => undo()}>Undo</Button>
        : null}

    </div>
  )
}

export default withUndo(withPeople(withFields(FormRedux)));

