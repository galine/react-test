import { select, takeLatest, put, call } from 'redux-saga/effects'
import {SET_FIELD, REMOVE_ALL, SAVE_FIELD, REMOVE_PERSON} from "../../Store/action-types";
import {setFields, setPeople} from "../../Store/action";

function* setFieldsSaga() {
  const fields = yield select(state => state.people.fields)
  localStorage.setItem('fields', JSON.stringify(fields));
}

function* loadFields() {
  const fields = localStorage.getItem('fields');
  if(fields) {
    yield put(setFields(JSON.parse(fields)))
  }
}

function* clearFields() {
  localStorage.removeItem('fields');
}

function* setPeopleSaga() {
  const people = yield select(state => state.people.people)
  localStorage.setItem('people', JSON.stringify(people));
}

function* loadPeople() {
  const people = localStorage.getItem('people');
  if(people) {
    yield put(setPeople(JSON.parse(people)))
  }
}

function* sagaPost() {
  yield takeLatest(SET_FIELD, setFieldsSaga);
  yield call(loadFields);
  yield takeLatest([REMOVE_ALL, SAVE_FIELD], clearFields);

  yield takeLatest([REMOVE_PERSON, SAVE_FIELD],  setPeopleSaga);
  yield call(loadPeople);

}

export default sagaPost;