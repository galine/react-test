import React from "react";
import withFields from "../../hoc/WithField";
import withPeople from "../../hoc/WithPeople";
import Button from '@material-ui/core/Button';
import InputUI  from '@material-ui/core/Input';
import {makeStyles} from "@material-ui/core";
import Select from "@material-ui/core/Select";

const useStyles = makeStyles({
  inputStyle: {
    width: 500,
    marginTop: 20
  }
})

const Input = ({value, index, setField, clearInput, name, type = 'text', id, label}) => {
  const style = useStyles();

  return (
    <div>
      <label className={value ? 'green' : null} htmlFor={id}>{label}</label> <br />

      <InputUI className={style.inputStyle} label="Standard" id={id} type={type} value={value} index={index} name={name} onChange={(e) => setField(index, name, e.target.value)}/>

      {value
        ?
        <Button  variant="contained" color="secondary" onClick={() => clearInput(index, name)}>✖</Button>
        : null
      }
    </div>
  )
}

export default withPeople(withFields(Input));
