import React from "react";
import "./App.css";
import { BrowserRouter, Route, NavLink } from 'react-router-dom';
import FormReduxContainer from "./Components/FormRedux/FormReduxContainer";
import List from "./Components/List/ListContainer";
import People from "./Components/People/People";
import Edit from "./Components/Edit/Edit";
import Post from "./Components/Post/Post";
import {makeStyles} from "@material-ui/core";

const useStyles = makeStyles({
  background: {
    textAlign: "center",
  }
});

function App() {
  const style = useStyles();

  return (
    <BrowserRouter>
      <div className={style.background}>
        <nav>
          <NavLink activeStyle={{fontWeight: "bold", color: "red"}} className="nav-link" to="/form">Form</NavLink>
          <NavLink activeStyle={{fontWeight: "bold", color: "red"}} className="nav-link" to="/list">List</NavLink>
          <NavLink activeStyle={{fontWeight: "bold", color: "red"}} className="nav-link" to="/people">People</NavLink>
          <NavLink activeStyle={{fontWeight: "bold", color: "red"}} className="nav-link" to="/post">Post</NavLink>
        </nav>
        <Route path='/form' exact component={FormReduxContainer} />
        <Route path='/list' exact component={List} />
        <Route path='/people' exact component={People} />
        <Route path='/people/edit/:index' children={<Edit />} />
        <Route path='/post' exact component={Post} />
      </div>
    </BrowserRouter>
  )
}

export default App;
