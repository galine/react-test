import {FETCH_POSTS, SET_REQUEST_DATA, TOGGLE_IS_LOADING} from "./action-types";

const initialState = {
  post: [],
  isLoading: false,
};

const post = (state = initialState, action ) => {
  switch (action.type) {
    case FETCH_POSTS:
      return {
        ...state,
        post: []
      };
    case SET_REQUEST_DATA:
      return {
        ...state,
        post: [...action.data]
    };
    case TOGGLE_IS_LOADING:
      return {
        ...state,
        isLoading: action.isLoading
      };

    default:
      return state;
  }
}

export default post;