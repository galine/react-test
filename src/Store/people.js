import {ADD_FIELD,
  REMOVE_FIELD,
  SET_FIELDS,
  SET_FIELD,
  SET_PEOPLE,
  CLEAR_INPUT,
  SAVE_FIELD,
  FILL_FIELD,
  REMOVE_ALL,
  UNDO,
  EDIT_FIELD,
  SET_EDIT_FIELD,
  REMOVE_PERSON,
} from "./action-types";

const initialState = {
  fields: [],
  people: [],
  undo: [],
};

const reducer = (state = initialState, action ) => {
  switch (action.type) {
    case ADD_FIELD:
      return {
        ...state,
        fields: [...state.fields, {firstname:'', lastname: '', email: '', password: ''}]
      };

    //setField(index, name, e.target.value)
    case SET_FIELD:
      return {
        ...state,
        fields: state.fields.map((field, i) => {
          return action.index === i
            ?
            {...field, [action.name]: action.value}
            : field
        })
      };
    case SET_FIELDS:
      return {
        ...state,
        fields: action.fields
      };
    case SET_PEOPLE:
      return {
        ...state,
        people: action.people
      };
    case REMOVE_FIELD:
      return {
        ...state,
        fields: state.fields.filter((field, i) => action.index !== i)
      };
    case CLEAR_INPUT:
      return {
        ...state,
        fields: state.fields.map((field, i) => {
          return ({...field, [action.name]: ''})
        })
      };
    case SAVE_FIELD:
      return {
        ...state,
        fields: [],
        people: [...state.people, ...state.fields],
      };
    case FILL_FIELD:
      state.fields[state.fields.length - 1] = state.people[action.index]
      return {
        ...state,
        fields: [...state.fields],
      };
    case REMOVE_ALL:
      return {
        ...state,
        undo: [...state.undo, ...state.fields],
        fields: [],
      };
    case UNDO:
      console.log(state.fields)
      return {
        ...state,
        fields: [...state.fields, ...state.undo],
        undo: [],
      };
    case EDIT_FIELD:
      return {
        ...state,
        people: state.people.map((person, i) => {
          return action.index === i
            ?
            {...person, [action.name]: action.value}
            : person
        })
      };
    case SET_EDIT_FIELD:
      state.people[action.index] = {...state.people[action.index], [action.name]: action.value}
      return {
        ...state,
        people: [...state.people]
      };
    case REMOVE_PERSON:
      return {
        ...state,
        people: state.people.filter((person, i) => action.index !== i)
      };

    default:
      return state;
  }
}

export default reducer;