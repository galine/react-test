import { createStore, applyMiddleware, combineReducers } from 'redux'
import createSagaMiddleware from 'redux-saga'
import {fork } from 'redux-saga/effects'
import mySaga from '../Components/FormRedux/saga'
import sagaPost from './sagaPost'
import { composeWithDevTools } from 'redux-devtools-extension';
import reducer from "./people";
import post from "./post";

const sagaMiddleware = createSagaMiddleware()

const reducers = combineReducers({
  people: reducer,
  post
});

const store = createStore(
  reducers,
  composeWithDevTools(
    applyMiddleware(sagaMiddleware)
  )
);

function* rootSaga() {
  yield fork(mySaga)
  yield fork(sagaPost)
}

sagaMiddleware.run(rootSaga)

export default store;
