import {ADD_FIELD,
  REMOVE_FIELD,
  SET_FIELD,
  SET_FIELDS,
  SET_PEOPLE,
  CLEAR_INPUT,
  SAVE_FIELD,
  FILL_FIELD,
  REMOVE_ALL,
  UNDO,
  EDIT_FIELD,
  SET_EDIT_FIELD,
  REMOVE_PERSON,
  FETCH_POSTS,
  SET_REQUEST_DATA,
  TOGGLE_IS_LOADING
} from "./action-types";

export const addNewField = () => ({
  type: ADD_FIELD
});

export const setField = (index, name, value) => ({
  type: SET_FIELD,
  index,
  name,
  value
});

export const setFields = (fields) => ({
  type: SET_FIELDS,
  fields
});

export const setPeople = (people) => ({
  type: SET_PEOPLE,
  people
});

export const removeField = (index) => ({
  type: REMOVE_FIELD,
  index
});

export const clearInput = (index, name) => ({
  type: CLEAR_INPUT,
  index,
  name
});

export const saveField = () => ({
  type: SAVE_FIELD
});

export const fillField = (index) => ({
  type: FILL_FIELD,
  index
});

export const removeAll = (index) => ({
  type: REMOVE_ALL,
  index
});

export const undo = (index) => ({
  type: UNDO,
  index
});

export const editField = (index, name, value) => ({
  type: EDIT_FIELD,
  index,
  name,
  value
});

export const setEditField = (index, name, value) => ({
  type: SET_EDIT_FIELD,
  name,
  value,
  index
});

export const removePerson = (index) => ({
  type: REMOVE_PERSON,
  index
});

export const fetchPosts = () => ({
  type: FETCH_POSTS,
});

export const setRequestData = (data) => ({
  type: SET_REQUEST_DATA,
  data
});

export const toggleIsLoading = (isLoading) => ({
  type: TOGGLE_IS_LOADING,
  isLoading
});
