import { select, takeLatest, put, call } from 'redux-saga/effects'
import {FETCH_POSTS, TOGGLE_IS_LOADING} from "./action-types";
import {setRequestData, toggleIsLoading} from "./action"
import {get} from "./fetch";


function* fetchRequest() {

  yield put(toggleIsLoading(true))

  const data = yield call(get, 'https://jsonplaceholder.typicode.com/posts')
  yield put(setRequestData(data))

  yield put(toggleIsLoading(false))
}

function* sagaPost() {
  yield takeLatest(FETCH_POSTS, fetchRequest);
}

export default sagaPost;