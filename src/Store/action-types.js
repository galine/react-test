export const ADD_FIELD = 'ADD_FIELD';
export const SET_FIELD = 'SET_FIELD';
export const SET_FIELDS = 'SET_FIELDS';
export const SET_PEOPLE = 'SET_PEOPLE';
export const REMOVE_FIELD = 'REMOVE_FIELD';
export const CLEAR_INPUT = 'CLEAR_INPUT';
export const SAVE_FIELD = 'SAVE_FIELD';
export const FILL_FIELD = 'FILL_FIELD';
export const REMOVE_ALL = 'REMOVE_ALL';
export const UNDO = 'UNDO';
export const EDIT_FIELD = 'EDIT_FIELD';
export const SET_EDIT_FIELD = 'SET_EDIT_FIELD';
export const REMOVE_PERSON = 'REMOVE_PERSON';

export const FETCH_POSTS = 'FETCH_POSTS';
export const SET_REQUEST_DATA = 'SET_REQUEST_DATA';
export const TOGGLE_IS_LOADING = 'TOGGLE_IS_LOADING';
